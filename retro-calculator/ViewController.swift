//
//  ViewController.swift
//  retro-calculator
//
//  Created by Marco Rodrigues on 15/02/2016.
//  Copyright © 2016 Marco Rodrigues. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    enum Operation: String {
        case Divide = "/"
        case Multiply = "*"
        case Sum = "+"
        case Subtract = "-"
        case Empty = ""
    }
    
    @IBOutlet weak var outputLbl: UILabel!
    
    var avBtnSound: AVAudioPlayer!
    
    var runningNumber: String = ""
    var valLeft: String = ""
    var valRight: String = ""
    var result: String = ""
    var currentOperation: Operation = Operation.Empty

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("btn", ofType: "wav")
        let soundUrl = NSURL(fileURLWithPath: path!)
        
        do {
            try avBtnSound = AVAudioPlayer(contentsOfURL: soundUrl)
            avBtnSound.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    @IBAction func clearPressed(btn: UIButton!)
    {
        playSound()
        runningNumber = ""
        valLeft = ""
        valRight = ""
        result = ""
        currentOperation = Operation.Empty
        outputLbl.text = "0.0"
    }
    
    @IBAction func numberPressed(btn: UIButton!)
    {
        playSound()
        addToRunningNumber(btn.tag)
    }
    
    @IBAction func onDividePressed(sender: AnyObject)
    {
        processOperation(Operation.Divide)
    }

    @IBAction func onMultiplyPressed(sender: AnyObject)
    {
        processOperation(Operation.Multiply)
    }
    
    @IBAction func onSubtractPressed(sender: AnyObject)
    {
        processOperation(Operation.Subtract)
    }
    
    @IBAction func onSumPressed(sender: AnyObject)
    {
        processOperation(Operation.Sum)
    }
    
    @IBAction func onEqualsPressed(sender: AnyObject)
    {
        processOperation(currentOperation)
    }
    
    func addToRunningNumber(number: Int) -> ()
    {
        runningNumber += "\(number)"
        outputLbl.text = runningNumber
    }
    
    func processOperation(operation: Operation) -> ()
    {
        playSound()
        if (currentOperation != Operation.Empty)
        {
            // user selected operator
            // but before it selected a number
            if runningNumber != ""
            {
                valRight = runningNumber
                if (currentOperation == Operation.Multiply)
                {
                    result = "\(Double(valLeft)! * Double(valRight)!)"
                }
                else if (currentOperation == Operation.Divide)
                {
                    result = "\(Double(valLeft)! / Double(valRight)!)"
                }
                else if (currentOperation == Operation.Subtract)
                {
                    result = "\(Double(valLeft)! - Double(valRight)!)"
                }
                else if (currentOperation == Operation.Sum)
                {
                    result = "\(Double(valLeft)! + Double(valRight)!)"
                }
                valLeft = result
                outputLbl.text = result
            }
            currentOperation = operation              
        }
        else
        {
            // this is the first time an operator has been pressed
            valLeft = runningNumber
            currentOperation = operation
        }
        runningNumber = ""
    }
    
    func playSound() -> ()
    {
        if (avBtnSound.playing)
        {
            avBtnSound.stop()
        }
        avBtnSound.play()
    }
    
}

